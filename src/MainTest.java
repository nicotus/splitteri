import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.List;

class MainTest {
    private static final String testSentence =
                    "Lorem ipsum dolor sit " +
                    "amet, consectetur adipiscing " +
                    "elit, sed do eiusmod " +
                    "tempor incididunt ut " +
                    "labore et dolore magna " +
                    "aliqua. Vitae congue " +
                    "eu consequat ac felis " +
                    "donec et. Tempus iaculis " +
                    "urna id volutpat lacus.";

    private static final List<String> testExpectedResults = Arrays.asList(
            "Lorem ipsum dolor sit",
            "amet, consectetur adipiscing",
            "elit, sed do eiusmod",
            "tempor incididunt ut",
            "labore et dolore magna",
            "aliqua. Vitae congue",
            "eu consequat ac felis",
            "donec et. Tempus iaculis",
            "urna id volutpat lacus.");

    private static final List<String> testExpectedResults2 = Arrays.asList(
            "Lorem ipsum dolor sit amet, consectetur ",
            "adipiscing elit, sed do eiusmod tempor incididunt",
            "ut labore et dolore magna aliqua. Vitae ",
            "congue eu consequat ac felis donec et. Tempus",
            "iaculis urna id volutpat lacus. "
    );

    @Test
    void splitTextToLinesWith20MaxLengthTest() {
        short testLineMaxLength = 20;

        List<String> results =  Main.splitTextToLines(testLineMaxLength, testSentence);

        for (String result : results) {
            System.out.println(result);
        }

        Assertions.assertEquals(9, results.size());

        for (String result : results) {
            Assertions.assertTrue(testExpectedResults.contains(result));
        }
    }

    @Test
    void splitTextToLinesWith40MaxLengthTest() {
        short testLineMaxLength = 40;

        List<String> results =  Main.splitTextToLines(testLineMaxLength, testSentence);

        for (String result : results) {
            System.out.println(result);
        }

        Assertions.assertEquals(5, results.size());

        for (String result : results) {
            Assertions.assertTrue(testExpectedResults2.contains(result));
        }
    }
}