import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> endResult = splitTextToLines((short) 40, sentence2);

        for (String result : endResult) {
            System.out.println(result);
        }
    }
    private static final String sentence2 =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor "
                    + "incididunt ut labore et dolore magna aliqua. Vitae congue eu consequat ac "
                    + "felis donec et. Tempus iaculis urna id volutpat lacus.";
    /**
     * NEW-TASK 1-A: Create a method that splits the given text to lines that are no longer than the
     * given line character length N. If a word is larger than the line length, it is returned
     * unmodified for that line.
     * <p>
     */
    static List<String> splitTextToLines(short lineMaxLength, String stringToSplit) {
        String[] words = stringToSplit.split(" ");
        List<String> rows = new ArrayList<>();
        StringBuilder row = new StringBuilder();

        short wordCount = 0;
        for (String word: words) {
            if (row.length() < lineMaxLength) {
                row.append(word);
                if(row.length() < lineMaxLength) row.append(" ");
            } else {
                rows.add(row.toString());
                row = new StringBuilder();
                row.append(word).append(" ");
            }
            wordCount++;

            if (wordCount == words.length) {
                rows.add(row.toString());
            }
        }
        return rows;
    }
}